#!/bin/sh
echo go AWS Go 

# Update parameters. Feel free to change this!
FILENAME=B02I11
DEVICESW=2.0.1




##########################################################
# WARNING: BE AWARE of changing everything after this line!
# Upgrading script
# go to HWsettings / SWsettings directory

if [ ! -d /home/piavita/application/settings ]; then
  # Control will enter here if $DIRECTORY doesn't exist.
  # OLD CODE STRUCTURE
  echo OLD CODE STRUCTURE
  cd /home/piavita/application
  VERSION=$(sed -n 's|.*"BasestationSW":"\([^"]*\)".*|\1|p' SWsettings.json)
else
  cd /home/piavita/application/settings
  echo NEW CODE STRUCTURE 2
  VERSION=$(sed -n 's|.*"BasestationSW": "\([^"]*\)".*|\1|p' SWsettings.json)
fi
UPDATESERVER=$(sed -n 's|.*"updateserver":"\([^"]*\)".*|\1|p' HWsettings.json)

# check if a software update exists
echo Compare this versions
echo $FILENAME 
echo $VERSION
if [ $FILENAME = $VERSION ];then
  echo no update
  exit 0
fi
cd /home/piavita/application

#mount -t type device dir
sudo mount -t ubifs ubi0:data /mnt/dataM
sleep 1
#change to mounted dataM partition
cd /mnt/dataM
#remove update script from mtd7 (Update) partition
echo remove update file form mtd7
sudo flash_eraseall /dev/mtd7
#remove old images
[ -f uImage ] && rm uImage
[ -f efusa7ull.dtb ] && rm efusa7ull.dtb
[ -f efusa7ul.dtb ] && rm efusa7ul.dtb
[ -f rootfs.ubifs ] && rm rootfs.ubifs
[ -f update.scr ] && rm update.scr

#download new images
echo try to download
#kernel
wget -T 20 https://bitbucket.org/piavita/com.piavita.basestation.images.b02e3/raw/$UPDATESERVER/releases/$FILENAME/uImage || exit 0
#device tree
wget -T 20 https://bitbucket.org/piavita/com.piavita.basestation.images.b02e3/raw/$UPDATESERVER/releases/$FILENAME/efusa7ull.dtb || exit 0
#rootfilesystem
wget -T 20 https://bitbucket.org/piavita/com.piavita.basestation.images.b02e3/raw/$UPDATESERVER/releases/$FILENAME/rootfs.ubifs || exit 0
#update script
wget -T 20 https://bitbucket.org/piavita/com.piavita.basestation.images.b02e3/raw/$UPDATESERVER/releases/$FILENAME/update.scr || exit 0
#force the system to commit the buffer cache to the Flash memory
sync -f /mnt/dataM
echo synced
sleep 5


cd /mnt/dataM/
#write update script to mtd7 partition
sudo nandwrite -p /dev/mtd7 update.scr
echo nand written
sleep 5
cd /home/piavita/application
#unmount /mnt/dataM
sudo umount /mnt/dataM/
echo update ready reboot NOW

cd /home/piavita/application

#force the system to commit the buffer cache to the Flash memory
#reboot
sync
sleep 5
sudo reboot -f
